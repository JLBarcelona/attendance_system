<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Users.index');
});


Route::get('/users', [UserController::class, 'index'])->name('user.index');
Route::get('/users/list', [UserController::class, 'list_users'])->name('user.list');
Route::post('/users/save', [UserController::class, 'save_users'])->name('user.save');
Route::delete('/users/delete/{id?}', [UserController::class, 'destroy_users'])->name('user.delete');
Route::get('/users/find/{id?}', [UserController::class, 'find_users'])->name('user.find');

// Route::controller(UserController::class)->group(function(){

// });

// Route::controller(UserController::class)->group(function () {