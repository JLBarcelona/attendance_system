<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    use HasFactory;
    
    protected $primaryKey = 'attendance_id';

    protected $table = 'attendances';
  
    protected $fillable = [
        'user_id',
        'date_attendance',
        'time_in',
        'time_out',
    ];


}
