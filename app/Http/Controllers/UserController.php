<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{
    public function index(){
    	return view('Users.index');
    }

    public function save_users(Request $request){
    	$user_id = $request->get('user_id');
    	$name = $request->get('name');  /* $request->get() is equivalent to $_POST['name']*/
    	$gender = $request->get('gender');
    	$email = $request->get('email');
        $password = $request->get('password');

    	$is_password_required = (!empty($user_id))? '' : 'required';

    	$validator =  Validator::make($request->all(), [	
			'name' => 'required',
			'gender' => 'required',
			'email' => 'required|email|unique:users,email,'.$user_id.',user_id',
			'password' => $is_password_required,
    	]);

    	if ($validator->fails()) {
	    	return response()->json(['status' => false, 'error' => $validator->errors()]);
    	}else{

    		if (!empty($user_id)) { /*Update*/
    			$user = User::where('user_id', $user_id)->first();
    			$user->name = $name;
				$user->gender = $gender;
				$user->email = $email;
				if (!empty($password)) {
					$user->password = $password;
				}
    			if ($user->save()) {
    				return response()->json(['status' => true, 'message' => 'User updated successfully!']);
    			}
    		}else{ /*Insert*/
	    			$user = new User;
					$user->name = $name;
					$user->gender = $gender;
					$user->email = $email;
					$user->password = $password;

    			if ($user->save()) {
    				return response()->json(['status' => true, 'message' => 'User added successfully!']);
    			}
    		}
	    	return response()->json(['status' => true, 'data' => $request->all()]);
    	}

    }

    public function list_users(Request $request){
        $keyword = $request->input('keyword');

        if(!empty($keyword)){
            $users = User::where('name', 'LIKE', '%' . $keyword . '%')->get();
        }else{
            $users = User::all(); 
        }
    	
    	return response()->json(['status' => true, 'data' => $users]);
    }

    public function destroy_users($userId){
        // $query = "DELETE FROM tbl_users where id="+$userid+"";
        $has_user = User::where('user_id', $userId);
        if ($has_user->count() > 0) {
            if ($has_user->delete()) {
                return response()->json(['status' => true, 'message' => 'User deleted successfully']);
            }
        }else{
            return response()->json(['status' => false, 'message' => 'User not found!']);
        }
    }

    public function find_users($id){
        $users = User::find($id);

        // $users = User::where('gender', 'female')->where('user_id', $id)->first();
        
        return response()->json(['status' => true, 'data' => $users]);

    }
}


// $user = User::where('user_id', $user_id)->update(['name' => $name,
// 					    			'gender' => $gender,
// 					    			'email' => $email,
// 					    			'password' => $password]);


// $user = User::insert(['name' => $name,
//   			'gender' => $gender,
//   			'email' => $email,
//   			'password' => $password]);