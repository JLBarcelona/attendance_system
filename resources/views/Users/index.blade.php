@extends('Layout.app')
@section('title', 'Users')

@section('css')
@endsection

@section('content')
<div class="row justify-content-center">
	<div class="col-sm-10">
		<form action="{{ route('user.save') }}" id="user_registration" class="needs-validation" novalidate>
			<div class="card">
				<div class="card-header">User Registration</div>
				<div class="card-body">
					<div class="row">
						<input type="hidden" name="user_id" id="user_id">
						<div class="col-sm-12 form-group mb-2">
							<label>Name</label>
							<input type="text" name="name" id="name" class="form-control" required="" placeholder="Enter firstname here">
							<div class="invalid-feedback" id="err_name"></div>
						</div>
						<div class="col-sm-12 form-group mb-2">
							<label>Gender</label>
							<select class="form-select" name="gender" id="gender" required="">
								<option value="" selected>Select Gender</option>
								<option value="male">Male</option>
								<option value="female">Female</option>
							</select>
							<div class="invalid-feedback" id="err_gender"></div>
						</div>
						<div class="col-sm-12 form-group mb-2">
							<label>Email</label>
							<input type="email" name="email" id="email" class="form-control" required="" placeholder="Enter valid email here">
							<div class="invalid-feedback" id="err_email"></div>
						</div>
						<div class="col-sm-12 form-group mb-2">
							<label>Password</label>
							<input type="password" name="password" id="password" class="form-control" required="" autocomplete="off" placeholder="Enter password here">
							<div class="invalid-feedback" id="err_password"></div>
						</div>
					</div>
				</div>
				<div class="card-footer">
					<button type="button" class="btn btn-dark" id="cancel_button">Cancel</button>
					<button type="submit" id="submit_btn" class="btn btn-success">Submit</button>
				</div>
			</div>
		</form>
		<div class="text-end">
					<input type="text" name="search" id="search" class="mt-5" placeholder="Search user here">
					<button type="button" id="search_button" class="btn-success">Search</button>
			</div>
		<table class="table table-primary mt-2">
			<thead>
				<tr>
					<th>ID</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Gender</th>
					<th>Option</th>
				</tr>
			</thead>
			<tbody id="user_table_data"></tbody>
		</table>
	</div>
</div>
@endsection

@section('script')
	<script type="text/javascript">
	list_users();

	function list_users($search=''){
		$.ajax({
			type:'get',
			url:"{{ route('user.list') }}?keyword="+$search,
			data:{},
			dataType:'json',
			success:function(response){
				//console.log(response);
				let responseData = response.data;
				let table_output = responseData.map(function(usr){
				let out = '';
					out += '<tr>';
					out += '<td>'+usr.user_id+'</td>';
					out += '<td>'+usr.name+'</td>';
					out += '<td>'+usr.gender+'</td>';
					out += '<td>';
					out += '<button class="btn btn-success" onclick="find_user(\''+usr.user_id+'\')">Edit</button> ';
					out += '<button class="btn btn-danger" onclick="destroy_users(\''+usr.user_id+'\')">Delete</button>';
					out += '</td>';
					out += '</tr>';
					return out;
				}).join('');
					$("#user_table_data").html(table_output);
			}

		});
	}

	$("#user_registration").on('submit', function(e){
		e.preventDefault();
		let url = $(this).attr('action');
		let formData = $(this).serialize();

		$.ajax({
			type:'post',
			url: url,
			data: formData,
			dataType : 'json',
			beforeSend:function(){
				$("#submit_btn").prop('disabled', true);
			},success:function(response){
				//console.log(response);
				$("#submit_btn").prop('disabled', false);
				if (response.status === true) {
					swal("Success", response.message, 'success');
					clear_form();
					list_users();
				}else{
					showValidator(response.error, 'user_registration');
				}
			},error:function(err){
				//console.log(err);
				$("#submit_btn").prop('disabled', false);
			}
		});
	});
	function destroy_users(id){
		// alert(id);
		let url = "{{ route('user.delete') }}"+'/'+id;
		let confirmation = confirm('Do you want to delete this user?');
		if (confirmation === true) {
			$.ajax({
				type:'delete',
				url:url,
				data:{},
				dataType:'json',
				success:function(response){
					//console.log(response);
					clear_form();
					list_users();
					window.scrollTo({ top: 0, behavior: 'smooth' });
					

					if (response.status === true) {
					swal("Success", 'Profile deleted successfully', 'success');
					}
				}	
			});
			}else{
				swal('You cancel the delete');
			}
	}
	function find_user(id){
	//	alert(id);
		let url = "{{ route('user.find') }}"+'/'+id;
		$.ajax({
			type:'get',
			url:url,
			data:{},
			dataType:'json',
			success:function(response){
				if (response.status === true) {
					$('#user_id').val(response.data.user_id);
					$('#name').val(response.data.name);
					$('#gender').val(response.data.gender);
					$('#email').val(response.data.email);
					$('#password').val(response.data.password);
					window.scrollTo({ top: 0, behavior: 'smooth' });
				}
			}
		});
	}
	function clear_form(){
		$('#user_id').val('');
		$('#name').val('');
		$('#gender').val('');
		$('#email').val('');
		$('#password').val('');
	}
	$('#cancel_button').on('click', function(response){
		clear_form();
	});

	$('#search_button').on('click', function(){
		var keyword = $('#search').val();
		//console.log(keyword);
		list_users(keyword);
	});
</script>
@endsection