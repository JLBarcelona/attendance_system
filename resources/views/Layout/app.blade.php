<!DOCTYPE html>
<html>
	<head>
		<title>@yield('title')</title>
		@include('Layout.header')
		@yield('css')
	</head>
	<body>
		<div class="container-fluid mt-3">
			@yield('content')
		</div>
	</body>
	@include('Layout.footer')
	@yield('script')
</html>