<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>

 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>

{{-- <?php echo '' ?>  equivalent for  {{ }} --}}
{{-- asset() locates the public folder --}}

 <script type="text/javascript" src="{{ asset('js/ajaxsetup.js') }}"></script>
 <script type="text/javascript" src="{{ asset('js/sweetalert.min.js') }}"></script>
 <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>